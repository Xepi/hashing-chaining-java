public class Hashing
{
	public Hashing(){} //Oppretter default construktor
	static SingelLinkedList [] hasharray; //Oppretter array

	public static void hashTable(int arraylength)
	{
		hasharray = new SingelLinkedList[arraylength]; //Oppretter array med lengde

		for(int i=0;i<arraylength;i++)
		{
			hasharray[i] = new SingelLinkedList();//fyller opp arrayen med objekter av SingelLinkedList
		}

	}

	public static void add(int arraylength, int element)//Metode for � legge til et element med verdi
	{
		int index = element % arraylength; //Modulo utregning for � finne riktig index
		hasharray[index].insertInFront(new Node(element, null));//kaller p� metode for � legge noden inn i listen (foran)

		for(int j = 0;j<arraylength;j++)
		{
			System.out.println("List " + j + ": " + hasharray[j]);//Skriver ut listene som g�r ut fra indexene i hasharrayen.
		}

	}
}