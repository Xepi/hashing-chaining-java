import java.util.Scanner;

public class TestHashing
{
	public static void main(String[]args)
	{
		Hashing myHash = new Hashing();//Oppretter objekt vha default konstruktor i klassen Hashing
		Scanner input = new Scanner(System.in);

		System.out.println("How long should the array be?");
		int arraylength = input.nextInt();
		myHash.hashTable(arraylength);//kaller p� metoden hashtable og snder inne brukern valgte lengde p� array.

		for(int i = 0;i<25;i++)//G�r igjennom 25 ganger for � tilsette 25 elementer
		{
			System.out.println("\nWhat element would you like to add? Current elements " + i);
			int element = input.nextInt();

			myHash.add(arraylength, element);//kaller p� metoden add i klassen Hashing som legger elementet i listen og skriver ut.
		}

		System.out.println("");
		double sum = 0.0;
		for(int j = 0;j<arraylength;j++)//for l�kke for � skrive ut loadfaktor i alle listene.
		{
			double loadfactor = ((double)myHash.hasharray[j].getElements() / 25);//bruker casting for � gj�re en int om til double
			System.out.println("List " + j + "'s loadfactor: " + loadfactor);//finner ogs� elementet inne i listen i indexe til hasharrayen
			sum = sum + loadfactor;//summerer total loadfaktor
		}
		System.out.println("\nTotal loadfactor is: " + sum);//skriver ut total loadfaktor
	}
}